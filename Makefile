# makefile of tqi

CXXFLAGS += -std=c++11 -O2 -g # -fverbose-asm
#COMPILER = clang++
COMPILER = g++
##############################################################################

OBJS=hint hstr #hchr

ALL		: $(OBJS)
	@echo ALL done

clean	:
	@rm -rf $(OBJS) htest h*.dSYM

####### program ###########################################################
hint 	: hash.cc HashList.h
	$(COMPILER) $(CXXFLAGS) $(LDFLAGS) -DINTEGER_VER=1 -o $@ hash.cc

hchr 	: hash.cc HashList.h
	$(COMPILER) $(CXXFLAGS) $(LDFLAGS) -DCHARPTR_VER=1 -o $@ hash.cc

hstr 	: hash.cc HashList.h
	$(COMPILER) $(CXXFLAGS) $(LDFLAGS) -DSTRING_VER=1 -o $@ hash.cc

htest	: htest.cc
	$(COMPILER) $(CXXFLAGS) $(LDFLAGS) -o $@ $<

##############################################################################
